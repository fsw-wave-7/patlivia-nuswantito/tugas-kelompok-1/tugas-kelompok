const router = require ('express').Router ()
const passport = require('../../lib/passport')
const restrict = require('../../middlewares/restrict')
const bodyParser = require('body-parser')

router.use(bodyParser.json())

// Controllers
const UserController = require ('../../controllers/api/userController')
const userController = new UserController()
const restrictApi = require('../../middlewares/restrict-api')
const {register} = require('../../controllers/api/authController')

// Register Page
router.get('/register' , (req, res) => res.render ('register'))
router.post('/register' , register)
// router.get("/whoami", restrict, whoami)
router.post('/v1/auth/login', userController.login)
// router.get('/v1/auth/whoami', restrictApi, auth.whoami)

module.exports = router

