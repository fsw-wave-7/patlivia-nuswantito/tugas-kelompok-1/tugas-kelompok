const router = require("express").Router();
const UserController = require("../../controllers/api/userController");
const userController = new UserController();

router.get("/", userController.getUser);
router.post("/adduser", userController.insertUser);
router.put("/edit/:id", userController.updateUser);
router.delete("/delete/:id", userController.deleteUser);

module.exports = router;


/*
get buat histori postingan + profil dia

post buat postingan


*/