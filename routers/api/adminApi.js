const router = require("express").Router();
const AdminController = require("../../controllers/api/adminController");
const adminController = new AdminController();



router.get("/", adminController.getAdmin);
router.post("/registeradmin", adminController.insertAdmin);
router.put("/edit/:id", adminController.updateAdmin);
router.delete("/delete/:id", adminController.deleteAdmin);

module.exports = router;
