'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Posting extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      
      Posting.associate = (models) => {
        Posting.belongsTo(models.User, {
          as: "Posting",
          foreignKey: "user_id",
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
      });
    };

    }
  };
  Posting.init({
    text: DataTypes.STRING,
    images: DataTypes.STRING,
    user_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Posting',
  });
  return Posting;
};