const express = require("express");
const app = express();
const port = 3000;
const session = require('express-session')
// const flash = require('express-flash')
const bodyParser = require("body-parser");
const dashboard = require("./routers/dashboard");
const adminApi = require("./routers/api/adminApi");
const userApi = require("./routers/api/userApi");
const registerApi = require("./routers/api/register");
const path = require("path");






// Load static file
app.use(express.static(path.join(__dirname, "public")));

// Pertama, setting request body parser
// (Ingat! Body parser harus ditaruh paling atas)
app.use(express.urlencoded({ extended: false }))

// Load express Session
app.use(session({
    secret: 'Buat ini jadi rahasia',
    resave: false,
    saveUninitialized: false
   }))

// Ketiga, setting passport 
// (sebelum router dan view engine)
const passport = require('./lib/passport')
app.use(passport.initialize())
app.use(passport.session())

// Passport JWT
const passportJwt = require('./lib/passport-jwt')
app.use(passportJwt.initialize())

// Keempat, setting flash
// app.use(flash())

// Set view engine
app.set("view engine", "ejs");

// DASHBOARD
app.use("/", dashboard);

// API;
const restrictApi = require('./middlewares/restrict-api')

app.use("/api/admin", adminApi);
app.use("/api/user",restrictApi, userApi);
app.use("/api/register", restrictApi, registerApi)

app.listen(port, console.log(`server sukses berjalan di port: ${port}`));
